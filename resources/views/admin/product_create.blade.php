@extends('admin.layout')
@section('title') {{ $pageTitle ? $pageTitle : '' }} | @parent @stop

@section('page-css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/select2.min.css') }}">
@endsection


@section('main')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $pageTitle ? $pageTitle : '' }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sectors</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="box">
                <!--<div class="box-header with-border">
                    <h3 class="box-title">{{ $pageTitle ? $pageTitle : '' }}</h3>
                </div> -->
                <div class="box-body">



                    {!! Form::open(['class' => 'form-horizontal']) !!}
                   
                    <div class="form-group {{ $errors->has('category')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Category *</label>

                        <div class="col-sm-7">
                            <select class="select2 form-control" name="category">
                                <option value="">Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                            {!! $errors->has('category')? '<p class="help-block"> '.$errors->first('category').' </p>':'' !!}
                        </div>
                    </div>
		    
		  	
                    <div class="form-group {{ $errors->has('brand')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Brand *</label>

                        <div class="col-sm-7">
                            <!--<select class="select2 form-control" name="brand">
                                <option value="">Select Brand</option>
                                @foreach($brands as $brand)
                                    <option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
                                @endforeach
                            </select>
                            -->
                            <input type="text" name="brand" class="form-control" value="{{ old('brand') }}" placeholder="Write here product brand">
                            {!! $errors->has('brand')? '<p class="help-block"> '.$errors->first('brand').' </p>':'' !!}
                        </div>
                    </div>
                 

                    <div class="form-group {{ $errors->has('product_name')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Product name *</label>

                        <div class="col-sm-7">
                            <input type="text" name="product_name" class="form-control" value="{{ old('product_name') }}" placeholder="Write here product name">
                            {!! $errors->has('product_name')? '<p class="help-block"> '.$errors->first('product_name').' </p>':'' !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->has('unite_price')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Unit price *</label>

                        <div class="col-sm-7">
                            <input type="text" name="unite_price" class="form-control" value="{{ old('unite_price') }}" placeholder="0">
                            {!! $errors->has('unite_price')? '<p class="help-block"> '.$errors->first('unite_price').' </p>':'' !!}
                        </div>
                    </div>

                    
                    <div class="form-group {{ $errors->has('product_model')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Product Model </label>

                        <div class="col-sm-7">
                            <input type="text" name="product_model" class="form-control" value="{{ old('product_model') }}" placeholder="Write here product Model">
                            {!! $errors->has('product_model')? '<p class="help-block"> '.$errors->first('product_model').' </p>':'' !!}
                        </div>
                    </div>

		   
                    <div class="form-group {{ $errors->has('description')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Description </label>

                        <div class="col-sm-7">
                            <textarea name="description" class="form-control" rows="5"> {{ old('description') }} </textarea>
                            {!! $errors->has('description')? '<p class="help-block"> '.$errors->first('description').' </p>':'' !!}
                        </div>
                    </div>



                    <div class="form-group {{ $errors->has('measure')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Measure</label>

                        <div class="col-sm-7">
                            <select class="select2 form-control" name="measure" id="measure">
                                <option value="">Select the kind of measure (Materials only)</option>
                                    <option value="GRAM">GRAM</option>
                                    <option value="KG">KG</option>
                                    <option value="TON">TON</option>

                                    <option value="ML">ML</option>
                                    <option value="LITER">LITER</option>
                                    <option value="BOTTLE">BOTTLE</option>

                                    <option value="CM">CM</option>
                                    <option value="METER">METER</option>
                                    <option value="M2">M2</option>
                                    <option value="M3">M3</option>
                                    <option value="BOX">BOX</option>

                                    <option value="PCS">PCS</option>
                                    <option value="RIM">RIM</option>
                                    <option value="BAG">BAG</option>
                                    <option value="SAK">SAK</option>
                                    <option value="SET">SET</option>     
                                    <option value="UNIT">UNIT</option>
                                    <option value="ROLL">ROLL</option>
                                    <option value="PACK">PACK</option>
                                    <option value="LUSIN">LUSIN</option>
                                    <option value="SHEET">SHEET</option>
                                    <option value="BATANG">BATANG</option>
                            </select>
                            {!! $errors->has('measure')? '<p class="help-block"> '.$errors->first('measure').' </p>':'' !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('weight')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Weight</label>
                        <div class="col-sm-7">
                            <input type="text" id="weight" name="weight" class="form-control" value="{{ old('weight') }}" placeholder="0.0">
                            {!! $errors->has('weight')? '<p class="help-block"> '.$errors->first('weight').' </p>':'' !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('large')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Large</label>

                        <div class="col-sm-7">
                            <input type="text" id="large" name="large" class="form-control" value="{{ old('large') }}" placeholder="0.0">
                            {!! $errors->has('large')? '<p class="help-block"> '.$errors->first('large').' </p>':'' !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('width')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Width</label>

                        <div class="col-sm-7">
                            <input type="text" id="width" name="width" class="form-control" value="{{ old('width') }}" placeholder="0.0">
                            {!! $errors->has('width')? '<p class="help-block"> '.$errors->first('width').' </p>':'' !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('height')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Height</label>

                        <div class="col-sm-7">
                            <input type="text" id="height" name="height" class="form-control" value="{{ old('height') }}" placeholder="0.0">
                            {!! $errors->has('height')? '<p class="help-block"> '.$errors->first('height').' </p>':'' !!}
                        </div>
                    </div>


                           
                    <div class="form-group">
                        <div class="col-sm-7 col-sm-offset-3">
                            <button type="submit" name="submit" class="btn btn-primary" required="required"><i class="fa fa-plus-square-o"></i> Add Product</button>
                        </div>
                    </div>

                    {!! Form::close() !!}






                </div><!-- /.box-body -->


            </div><!-- /.box -->



        </div> <!-- /.col -->
    </div>
    <!-- /.row -->


</section><!-- /.content -->


@endsection

@section('page-js')
    <!-- Select2 -->
    <script src="{{ asset('assets/admin/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $(function () {
            $(".select2").select2();

            $('#addAttribute').click(function(){
                $.ajax({
                    type : 'GET',
                    url : '{{ route('add_product_attribute') }}',
                    success : function(data){
                        $('#attributeContainer').append(data);
                    }
                });
            });
        });

        $('body').on('click', 'a.attributeDelBtn', function(){
            $(this).closest('div.form-group').removeClass( "form-group" ).html('');
        });
    </script>


@endsection

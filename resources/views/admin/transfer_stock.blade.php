@extends('admin.layout')
@section('title') {{ empty($pageTitle) ? '' : $pageTitle }} | @parent @stop

@section('page-css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/select2.min.css') }}">
@endsection

@section('main')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ empty($pageTitle) ? '' : $pageTitle }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sectors</li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ empty($pageTitle) ? '' : $pageTitle }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    {!! Form::open(['class' => 'form-horizontal']) !!}

                    <div class="form-group {{ $errors->has('user')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Employee *</label>

                        <div class="col-sm-7">
                            <select class="select2 form-control" name="employee" id="employee">
                                <option value="">Select Employee</option>
                                @foreach($users as $user)
                                    <option value="{{ $user->photo }}">{{$user->first_name}}</option>
                                    
                                @endforeach
                            </select>
                            {!! $errors->has('user')? '<p class="help-block"> '.$errors->first('user').' </p>':'' !!}
                        </div>
                    </div>



                    <div class="form-group {{ $errors->has('shop')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Sector *</label>

                        <div class="col-sm-7">
                            <select class="select2 form-control" name="shop">
                                <option value="">Select Sector</option>
                                @foreach($shops as $shop)
                                    <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                                @endforeach
                            </select>
                            {!! $errors->has('shop')? '<p class="help-block"> '.$errors->first('shop').' </p>':'' !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->has('product')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Product *</label>

                        <div class="col-sm-7">
                            <select class="select2 form-control" name="product" id="product">
                                <option value="">Select Product</option>
                                @foreach($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->product_name }}</option>         
                                @endforeach
                            </select>
                            {!! $errors->has('product')? '<p class="help-block"> '.$errors->first('product').' </p>':'' !!}
                        </div>
                    </div>
                    
                
                    

                    <div class="form-group {{ $errors->has('total_product')? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Total Product </label>

                        <div class="col-sm-7">
                            <!--<input type="text" name="total_product" class="form-control" value="{{ old('total_product') }}" placeholder="Product Model">-->
                            <input type="text" name="total_product" class="form-control" value="1" placeholder="Product Model" disabled="true">
                            {!! $errors->has('total_product')? '<p class="help-block"> '.$errors->first('total_product').' </p>':'' !!}
                        </div>
                    </div>


                    <label class="control-label col-sm-3">User/Product Images </label>
                        <div class="col-sm-7">
                            <img id="user_image"  class="user-image" alt="**User without image yet" height="250" width="250">
                            <img id="product_image"  class="user-image" alt="**Product without image yet" height="250" width="250">
                        </div>
                    </div>

                    <!--
                    <label class="control-label col-sm-3">Product Image </label>
                        <div class="col-sm-7">
                            <img id="product_image"  class="user-image" alt="**Product without image yet" height="300" width="300">
                        </div>
                    </div>
                    -->

                    <div class="form-group">
                        <div class="col-sm-7 col-sm-offset-3">
                            <button type="submit" name="submit" class="btn btn-primary" required="required"><i class="fa fa-plus-square-o"></i> Transfer stock to Sector</button>
                        </div>
                    </div>

                    {!! Form::close() !!}






                </div><!-- /.box-body -->


            </div><!-- /.box -->



        </div> <!-- /.col -->
    </div>
    <!-- /.row -->

</section><!-- /.content -->


@endsection

@section('page-js')
    
    <script src="{{ asset('assets/admin/plugins/select2/select2.full.min.js') }}"></script>
    
    <script>
        $(function () {
            $(".select2").select2();
        });
    </script>
    
    <script>
        $('#employee').on('change', function(e){
            var select = $(this), form = select.closest('form');
            var user_photo = $('select[name="employee"]').val();
            //var url = 'http://fastfitnessventas.gn.cl/uploads/photo/'+user_photo;
            //$('#user_image').attr('src', url);
            $('#user_image').attr('src', `{{URL::asset('uploads/photo/`+user_photo+`')}}`);
            form.submit();
        });
    </script>
    
    <script>
        $('#product').on('change', function(e){
            var select = $(this), form = select.closest('form');
            var product_id = $('select[name="product"]').val();
                        
            $('#product_image').attr('src', `{{URL::asset('uploads/photo/`+product_id+`/`+product_id+`.png')}}`);

            form.submit();
        });
    </script>
 
@endsection
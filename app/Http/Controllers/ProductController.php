<?php


namespace App\Http\Controllers;

use App\Activity;
use App\Brand;
use App\Category;
use App\Product;
use App\Product_attribute;
use App\Stock;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use yajra\Datatables\Datatables;

use SimpleSoftwareIO\QrCode\Facades\QrCode;
use SimpleSoftwareIO\QrCode\Generator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Products';
        return view('admin.product_index', compact('pageTitle'));
    }

    public function indexData()
    {
        $products = Product::orderBy('id', 'desc')->select('id', 'photo','category_id','product_name', 'product_model', 'brand_id', 'unite_price', 'measure', 'created_at')->with('category', 'brand');
        return Datatables::of($products)
            /*->editColumn('photo', function($product){
                return "";
            })*/
            ->editColumn('product_name', function($product){
                return $product->product_name;
            })
            ->editColumn('brand_id', function($product){
                //return $product->brand->brand_name;
                return $product->brand_id;
            })
            ->editColumn('category_id', function($product){
                return $product->category->category_name;
            })
            ->editColumn('created_at', function($product){
                return '<span title="'.$product->created_at->format('F d, Y').'" data-toggle="tooltip" data-placement="top"> '.$product->created_at->diffForHumans().' </span>';
            })
            ->addColumn('stock', function($product){
                return $product->stock_available();
            })
            ->addColumn('actions', function($product){
                $button = '<a href="'.route('view_product', $product->id).'" class="btn btn-success" title="View" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i> </a>';
                $button .= '<a href="'.route('edit_product', $product->id).'" class="btn btn-info" title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i> </a>';
                $button .= '<a href="javascript:;" class="btn btn-danger deleteBrands" title="Delete" data-toggle="tooltip" data-placement="top" data-id="'.$product->id.'"><i class="fa fa-trash-o"></i> </a>';
                return $button;
            })
            ->removeColumn('id')
            ->removeColumn('brand')
            ->removeColumn('category')
            ->removeColumn('photo')
            ->make();
    }

    public function indexShop()
    {
        $pageTitle = 'Products';
        return view('user_admin.product_index', compact('pageTitle'));
    }

    public function indexShopData()
    {
        $products = Product::orderBy('id', 'desc')->select('id', 'product_name', 'product_model', 'brand_id','category_id')->with('category', 'brand');
        return Datatables::of($products)
            ->editColumn('brand_id', function($product){
                //return $product->brand->brand_name;
                return $product->brand_id;
            })
            ->editColumn('category_id', function($product){
                return $product->category->category_name;
            })
            ->addColumn('stock', function($product){
                return $product->stock_available();
            })
            ->removeColumn('id')
            ->removeColumn('brand')
            ->removeColumn('category')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Create Product';
        $categories = Category::all();
        $brands = Brand::all();
        return view('admin.product_create', compact('pageTitle', 'categories', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'product_name'  => 'required',
            'unite_price'  => 'required',
        ];

        //Dynamic Product Attribute Save into session
        $attribute_names = $request->input('attribute_name');
        $attribute_values = $request->input('attribute_value');

        $old_product_attribute['attribute_name'] = $attribute_names;
        $old_product_attribute['attribute_value'] = $attribute_values;

        session(['old_product_attribute' => $old_product_attribute]);

        //Now validate all
        $this->validate($request, $rules);

        $user = Auth::user();

        $category       = $request->input('category');
        $brand          = $request->input('brand');
        $product_name   = $request->input('product_name');
        $unite_price    = $request->input('unite_price');
        $product_model  = $request->input('product_model');
        $description    = $request->input('description');

        $measure    = $request->input('measure');
        $weight    = $request->input('weight');
        $large    = $request->input('large');
        $width    = $request->input('width');
        $height    = $request->input('height');



        $data = [
            'user_id'       => $user->id,
            'category_id'   => $category,
            'brand_id'      => $brand,
            'product_name'  => $product_name,
            'unite_price'   => $unite_price,
            'product_model' => $product_model,
            'description'   => $description,

            'measure'   => $measure,
            'weight'   => $weight,
            'large'   => $large,
            'width'   => $width,
            'height'   => $height,
        ];

        $create = Product::create($data);
        if($create)
        {
            //Now Additional Save Attribute
            $count_attribute_names = count((is_countable($attribute_names)?$attribute_names:[]));
            $product_attributes = [];
            for($i = 0; $i < $count_attribute_names; $i++)
            {
                $attribute_name = trim($attribute_names[$i]);
                if($attribute_name != '')
                {
                    $attributeData = [
                        'product_id'    => $create->id,
                        'attribute_name'=> $attribute_name,
                        'attribute_value'=> $attribute_values[$i]
                    ];
                    $createAttribute = Product_attribute::create($attributeData);
                    if($createAttribute)
                    {
                        $createAttribute -> c_order = $createAttribute->id;
                        $createAttribute->save();
                    }
                }
            }
            $request->session()->forget('old_product_attribute');

            //Add Activity
            Activity::create(['user_id' => $user->id, 'activity' => 'You have added '.$product_name. '  product']);
            $this->store_stock($request,$create->id);

            return redirect()->back()->with('success', 'Product create success');
        }
        return redirect()->back()->with('error', 'Something went wrong, please try again');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_stock(Request $request, Int $product_id)
    {
        $rules = [
            'unite_price'   => 'required',
        ];

        //determine if stock transfer to shop
        $shop_id = 0;
        $shop_base_activity = '';
        $unit_price = $request->input('unite_price');

        if(array_has($request->input(), 'shop')){
            $rules['shop'] = 'required';
            unset($rules['unite_price']);
            $shop_id = $request->input('shop');
            $unit_price = 0;

            if( ! empty($shop_id)){
                $shop_detail = Shop::find($shop_id);
                $shop_base_activity = ' to '.$shop_detail->name;
            }
        }

        $this->validate($request, $rules);

        $user = Auth::user();
        //$product_id = $request->input('product');

        $stockData = [
            'shop_id'       => $shop_id,
            'product_id'    => $product_id,
            'unite_price'   => 0,
            'total_product' => 1,
            'user_id'       => $user->id,
        ];

        $create = Stock::create($stockData);

        if($create){
            $product = Product::find($product_id);
                //Add Activity
                Activity::create(['user_id' => $user->id, 'activity' => 'You have added '.$stockData['total_product']. '  '.$product->product_name. $shop_base_activity ]);
                return redirect()->back()->with('success', 'Stock Added success');
        }
        return redirect()->back()->with('error', 'Something went wrong, please try again');

    }





    public function addProductAttribute()
    {
        return view('ajax.product_attribute');
    }

    public function deleteProductAttribute(Request $request)
    {
        $attrID = $request->input('attrID');
        Product_attribute::destroy($attrID);
        return ['status' => 1];
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pageTitle = 'Show Product';
        $product = Product::find($id);
        return view('admin.product_show', compact('product', 'pageTitle'));
    }


    public function getQR($id)
    {
        $qrcode = new Generator;
        return $qrcode->format('svg')->size(350)->color(0, 0, 0)->generate($id);
        //return (string)QrCode::format('svg')->size(350)->color(255, 0, 0)->generate("CODE: ". $id);
    }


    public function adminProductImageEdit($id)
    {
        $pageTitle = 'Product Image Edit';
        $product = Product::find($id);
        return view('admin.product_image_edit', compact('product', 'pageTitle'));
    }

    public function productImageEditPost(Request $request, $id)
    {
        $product = Product::find($id);

        //Profile Thumb
        $directory = public_path('uploads/photo/'.$id);

        if (!file_exists($directory)) mkdir($directory, 0775, true);
        //File unique name
        //$photoName = strtolower(str_random(7) . substr(time(), 4));
        $photoName = $id;

        //Verifying File Presence
        if ($request->hasFile('product_photo')) {
            //verifying file is valid
            if ($request->file('product_photo')->isValid()) {
                $ext = $request->file('product_photo')->getClientOriginalExtension();
                $imgNewName = $photoName . '.' . $ext;
                $is_upload = $request->file('product_photo')->move($directory, $imgNewName);

                if ($is_upload) {
                    //delete previous one
                    if(file_exists($product->photo != '' && $directory.$product->photo)) unlink($directory.$product->photo);
                    //Saved in earning table
                    $product->photo = $imgNewName;
                } //is file upload
            }
        }

        $product->save();
        return redirect()->back()->with('success', 'Product image edit success');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pageTitle = 'Edit Product';
        $categories = Category::all();
        $brands = Brand::all();
        $product = Product::find($id);
        return view('admin.product_edit', compact('pageTitle', 'categories', 'brands', 'product'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'category'  => 'required',
            'brand'  => 'required',
            'product_name'  => 'required',
            'unite_price'  => 'required',
        ];

        //Dynamic Product Attribute Save into session
        $attribute_names = $request->input('attribute_name');
        $attribute_values = $request->input('attribute_value');

        $attribute_name_edit = $request->input('attribute_name_edit');
        $attribute_values_edit = $request->input('attribute_value_edit');
        $attr_ids = $request->input('attr_ids');

        $old_product_attribute['attribute_name'] = $attribute_names;
        $old_product_attribute['attribute_value'] = $attribute_values;

        session(['old_product_attribute' => $old_product_attribute]);

        //Now validate all
        $this->validate($request, $rules);

        $user = Auth::user();


        $category       = $request->input('category');
        $brand          = $request->input('brand');
        $product_name   = $request->input('product_name');
        $unite_price    = $request->input('unite_price');
        $product_model  = $request->input('product_model');
        $description    = $request->input('description');

        $data = [
            'user_id'       => $user->id,
            'category_id'   => $category,
            'brand_id'      => $brand,
            'product_name'  => $product_name,
            'unite_price'   => $unite_price,
            'product_model' => $product_model,
            'description'   => $description,
        ];

        $create = Product::where('id', $id)->update($data);
        if($create)
        {
            //Now Additional Save Attribute
            $count_attribute_names = count((is_countable($attribute_names)?$attribute_names:[]));
            //$count_attribute_names = count($attribute_names);

            //First update previous attributes belongs with this product
            $count_attribute_name_edit = count((is_countable($attribute_name_edit)?$attribute_name_edit:[]));
            if($count_attribute_name_edit > 0)
            {
                for($i = 0; $i < count($attribute_name_edit); $i++)
                {
                    $attributeData = [
                        'attribute_name'    => $attribute_name_edit[$i],
                        'attribute_value'   => $attribute_values_edit[$i],
                        'c_order'           => $attr_ids[$i]
                    ];
                    Product_attribute::where('id', $attr_ids[$i])->update($attributeData);
                }
            }

            //Save new attributes
            if($count_attribute_names > 0){
                $product_attributes = [];
                for($i = 0; $i < $count_attribute_names; $i++)
                {
                    $attribute_name = trim($attribute_names[$i]);
                    if($attribute_name != '')
                    {
                        $attributeData = [
                            'product_id'    => $id,
                            'attribute_name'=> $attribute_name,
                            'attribute_value'=> $attribute_values[$i]
                        ];
                        $createAttribute = Product_attribute::create($attributeData);
                        if($createAttribute)
                        {
                            $createAttribute -> c_order = $createAttribute->id;
                            $createAttribute->save();
                        }
                    }
                }
                $request->session()->forget('old_product_attribute');
            }

            //Add Activity
            Activity::create(['user_id' => $user->id, 'activity' => 'You have updated '.$product_name. '  product']);

            return redirect()->back()->with('success', 'Product update success');
        }
        return redirect()->back()->with('error', 'Something went wrong, please try again');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = Auth::user();
        $product_id = $request->input('product_id');
        $response['status'] = 0;
        $product = Product::find($product_id);
        $product_name = $product->product_name;
        $destroy = $product->delete();

        if($destroy)
        {
            $response['status'] = 1;
            $response['msg']    = 'Success';
        }
        Activity::create(['user_id' => $user->id, 'activity' => 'You have deleted '.$product_name. '  product']);
        return $response;
    }
}

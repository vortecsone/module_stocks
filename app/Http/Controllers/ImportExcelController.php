<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;

class ImportExcelController extends Controller
{
    function index()
    {
        $data = DB::table('products')->orderBy('id', 'DESC')->get();
        return view('admin.import_excel', compact('data'));
    }

    function indexUser()
    {
        $data = DB::table('products')->orderBy('id', 'DESC')->get();
        return view('user_admin.import_excel', compact('data'));
    }

    function import(Request $request)
    {
        $this->validate($request, [
            'select_file'  => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if($data->count() > 0)
        {
            foreach($data->toArray() as $key => $value)
            {
                    $insert_data[] = array(
                        'code' => 'codetest',
                        'user_id' => 1,
                        'product_name'  => $value['product_name'],
                        'product_model'   => $value['product_model'],
                        'description'   => $value['description'],
                        'brand_id'    => $value['brand_id'],
                        'category_id'  => $value['category_id'],
                        'unite_price'   => $value['unite_price']
                    );
            }

            if(!empty($insert_data))
                DB::table('products')->insert($insert_data);
        }
        return back()->with('success', 'Excel Data Imported successfully.');
    }
}
